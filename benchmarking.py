import subprocess
import itertools
import re
import json
import traceback
from pathlib import Path

# General construction layout of the docker files
player_base_file = './docker/player_base.Dockerfile'
player_file = './docker/player.Dockerfile'
dockerComposeFile = 'Docker.Compose'

logFile = './data/log'

originalCircuit = 'benchmark_mobilenet-v1_0.25_128-0-1-conv2ds-cisc'
circuitWithMoreTriples = 'benchmark_mobilenet_more_triples-v1_0.25_128-0-1-conv2ds-cisc'

moreTriplesExt = '_more_triples'

header = """version: '2.2'
services:
"""

playerBuild = """  $Player$:
    cpus: 2
    build:
      context: .
      dockerfile: """ + str(player_file) + """
    cap_add:
      - NET_ADMIN
      - NET_RAW
    command: $Commands$
    container_name: $Player$
"""

BASE_PATH = str(Path(__file__).parent.absolute())
DATA_FOLDER = BASE_PATH + '/data/'
RUN_DATA_FILE = DATA_FOLDER + 'output.data'

possPlayers = [3]
possPrimes = [128]
possOnlineOnly = [False]
possNetworkDelay = [False]
possAddedTriples = [False]

resetsPerSimulation = 1

latencyWAN = "50"  					# Latency (in milliseconds) to use in WAN setting
bandwithWAN = "51200"  				# Maximum bandwith (in kbits) to use in WAN setting

verbose = True


def create_conf_files(numPlayers, prime, onlineOnly, addedTriples, networkDelay):
    player_config = generate_docker_player_configuration(numPlayers, prime)
    write_file(player_file, player_config)

    compose_config = generate_docker_compose_configuration(numPlayers, onlineOnly, addedTriples, networkDelay)
    write_file(dockerComposeFile, compose_config)


def generate_docker_player_configuration(numPlayers, prime):
    with open(player_base_file, 'r') as file:
        base_file = file.read()

    dockerFile = base_file.replace("$NUM_PLAYERS$", str(numPlayers))
    dockerFile = dockerFile.replace("$PRIME_FIELD$", str(prime))

    return dockerFile


def generate_docker_compose_configuration(numPlayers, onlineOnly, addedTriples, networkDelay):
    arguments = combineArguments(numPlayers, onlineOnly, addedTriples, networkDelay)
    dockerData = header
    for i in range(numPlayers):
        playerData = playerBuild
        currIndex = i
        currPlayer = 'player_' + str(currIndex + 1)
        argumentStr = str(arguments.replace("$PLAYER_INDEX$", str(currIndex)).replace("$Player$", currPlayer))

        playerData = playerData.replace('$Player$', currPlayer)
        playerData = playerData.replace('$Commands$', argumentStr)
        dockerData += playerData
    return dockerData


def write_file(file, data, append=False):
    if append:
        flag = 'a'
    else:
        flag = 'w'

    with open(file, flag) as file:
        file.write(data)


def combineArguments(numPlayers, onlineOnly, addedTriples, networkDelay):
    arguments = "iptables -A OUTPUT ! -d $Player$ -p tcp &&"

    # Introduce artificial network characteristics when simulating the WAN setting (and let it take effect (ping 5 times))
    if networkDelay:
        arguments += "tc qdisc add dev eth0 root netem delay " + \
            str(latencyWAN) + "ms rate " + str(bandwithWAN) + "kbit && ping player_1 -c 5 >> \dev\zero &&"

    # Evaluate the MobileNet neural network (with or without LowGear preprocessing)
    arguments += "cd /home/MP-SPDZ/"
    # for i in range(runsPerSimulation):
    arguments += "&& ./cowgear-party.x -N " + \
        str(numPlayers) + " -T -l 40 -h player_1 -pn 5001 -p $PLAYER_INDEX$ "
    if onlineOnly:
        arguments += "-F "

    if addedTriples:
        arguments += circuitWithMoreTriples
    else:
        arguments += originalCircuit

    bash_command = 'bash -c "' + arguments + '&& iptables -n -L -v -x"'
    return bash_command


def resetDocker():
    subprocess.run(["docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)"],
                   shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def parse_output(output):
    data = {"Run Time": {}, "Send Bytes": {}}
    for line in output.split('\n'):
        if "Time = " in line:
            ANSI_CLEANER = re.compile(r"(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]")
            clean_string = ANSI_CLEANER.sub("", line)

            time = float(re.findall("[0-9]+\.*[0-9]*", clean_string.split("Time = ")[1])[0]) * 1000
            player = str(re.findall("[player\_[0-9]+", clean_string)[0])

            data["Run Time"][player] = [time]

        if "tcp  --  *" in line:
            ANSI_CLEANER = re.compile(r"(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]")
            clean_string = ANSI_CLEANER.sub("", line)

            sendBytes = int(re.findall("[0-9]+", clean_string)[2])
            player = str(re.findall("[player\_[0-9]+", clean_string)[0])

            data["Send Bytes"][player] = sendBytes

    return data


def run_docker_configuration(networkDelay):
    # Reset the docker and optionally traffic forwarding service
    resetDocker()

    # Run docker-compose on the dockerfile
    if verbose:
        subprocess.run(["docker-compose", "-f", str(dockerComposeFile),
                        "up", "--build", "--abort-on-container-exit"])
    else:
        output = subprocess.run(["docker-compose", "-f", str(dockerComposeFile),
                                 "up", "--build", "--abort-on-container-exit"], capture_output=True).stdout.decode()
    write_file(logFile, "\n\n\n" + output, True)
    run_data = parse_output(output)
    return run_data


def run_single_simulation(numPlayers, prime, onlineOnly, addedTriples, networkDelay):
    create_conf_files(numPlayers, prime, onlineOnly, addedTriples, networkDelay)

    run_data = run_docker_configuration(networkDelay)
    return run_data


def run_multiple_simulations(numPlayers, prime, onlineOnly, addedTriples, networkDelay):
    all_times = {}
    for i in range(resetsPerSimulation):
        run_data = run_single_simulation(numPlayers, prime, onlineOnly, addedTriples, networkDelay)
        all_times["run_" + str(i + 1)] = run_data

    return all_times


def run_all_simulations():
    allCombinationsOfParamters = itertools.product(possPlayers,
                                                   possPrimes,
                                                   possOnlineOnly,
                                                   possAddedTriples,
                                                   possNetworkDelay)

    for numPlayers, prime, onlineOnly, addedTriples, networkDelay in allCombinationsOfParamters:
        resetDocker()
        run_data = run_multiple_simulations(numPlayers, prime, onlineOnly,  addedTriples, networkDelay)

        process_data(numPlayers, prime, onlineOnly, addedTriples, networkDelay, run_data)


def process_data(numPlayers, prime, onlineOnly, addedTriples, networkDelay, run_data):
    data_dict = {"numPlayers": numPlayers,
                 "prime": prime, "onlineOnly": onlineOnly, "addedTriples": addedTriples, "networkDelay": networkDelay, "data": run_data}

    with open(RUN_DATA_FILE, 'a+') as file:
        file.write(json.dumps(data_dict) + "\n")


if __name__ == '__main__':
    try:
        run_all_simulations()
    except:
        trace = traceback.format_exc()
        write_file(logFile, "\n\n\n" + trace, True)
