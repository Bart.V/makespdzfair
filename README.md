# Fair SPDZ
## General
- TODO

### Objective
- TODO

## Dependencies
To run the example you need to have the following installed
* Python3 (Tested on Python3.8)
* Docker
* Docker-compose


## Examples
- TODO

### Run
To run the example configure the variables in benchmarking.py and run the following command:
```console
$ python3.8 benchmarking.py
```