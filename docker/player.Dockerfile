#Setup toolchain, environment and files
FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive
ENV BASE_DIR="/home/MP-SPDZ/"
RUN apt-get update && apt-get install iptables nano iputils-ping dnsutils libntl-dev git automake build-essential git libboost-dev libboost-thread-dev libsodium-dev libssl-dev libtool m4 python3 texinfo yasm iproute2 -y
RUN cd /home/ && git clone https://github.com/data61/MP-SPDZ.git
ADD data/CONFIG $BASE_DIR
ADD data/MobileNet_Input/ ${BASE_DIR}Player-Data/

#Compile necessary libraries and protocols
RUN cd $BASE_DIR && make -j 8 tldr
RUN cd $BASE_DIR && make -j 8 cowgear-party.x
RUN cd $BASE_DIR && make -j 8 Fake-Offline.x


#Compile MobileNet neural network
RUN echo 'test'
RUN cd $BASE_DIR && ./compile.py -F 128 -D benchmark_mobilenet v1_0.25_128 0 1 conv2ds cisc
RUN cd $BASE_DIR && echo 'triples = 501\na = Array(triples, sint)\nfor i in range(triples):\n    a[i] = sint(0) * sint(0)\n' > Programs/Source/benchmark_mobilenet_more_triples.mpc
RUN cd $BASE_DIR && cat Programs/Source/benchmark_mobilenet.mpc >> Programs/Source/benchmark_mobilenet_more_triples.mpc
RUN cd $BASE_DIR && ./compile.py -F 128 -D benchmark_mobilenet_more_triples v1_0.25_128 0 1 conv2ds cisc
RUN cd $BASE_DIR $$ ./Fake-Offline.x

RUN apt install nethogs wget -y
RUN cd $BASE_DIR && ./compile.py tutorial
#Generate fake offline data for online only benchmarking
RUN cd $BASE_DIR && Scripts/setup-online.sh 3 128 0
